﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlazorApp3.Shared
{
    public class Vehicle
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
